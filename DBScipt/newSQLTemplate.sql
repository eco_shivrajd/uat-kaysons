/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Shivraj.Dakore
 * Created: 4 Jun, 2018
 */
updated files

brand-add.php
brands.php
category-add.php
categories.php
product-add.php
product.php


--
new files
--
new_order_from_stockist.php
ajax_addtokart_for_stockist.php



CREATE TABLE `tbl_customer_orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `current_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_var_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_customer_orders`
--
ALTER TABLE `tbl_customer_orders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_customer_orders`
--
ALTER TABLE `tbl_customer_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_customer_orders` ADD `order_date` TIMESTAMP NOT NULL AFTER `product_quantity`;

ALTER TABLE `tbl_customer_orders` ADD `order_status` VARCHAR(255) NOT NULL AFTER `order_date`;

ALTER TABLE `tbl_customer_orders` ADD `product_price` DECIMAL(10,2) NOT NULL AFTER `product_var_id`;

ALTER TABLE `tbl_customer_orders` ADD `total_cost` DECIMAL(10,2) NOT NULL AFTER `product_quantity`, ADD `total_cost_with_tax` DECIMAL(10,2) NOT NULL AFTER `total_cost`;

ALTER TABLE `tbl_user` CHANGE `user_type` `user_type` ENUM('Admin','Superstockist','Distributor','SalesPerson','DeliveryPerson','Accountant','DeliveryChannelPerson') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;


CREATE TABLE `tbl_dcp_cartons` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_var_id` int(11) NOT NULL,
  `qnty` int(11) NOT NULL,
  `carton_added_date` datetime NOT NULL,
  `isdeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_dcp_cartons`
--
ALTER TABLE `tbl_dcp_cartons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_dcp_cartons`
--
ALTER TABLE `tbl_dcp_cartons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_product_variant` ADD `carton_quantities` INT NOT NULL AFTER `sgst`;

ALTER TABLE `tbl_product_variant` ADD `price_ss` FLOAT(8,2) NOT NULL AFTER `price`, ADD `price_dcp` FLOAT(8,2) NOT NULL AFTER `price_ss`, ADD `margin_ss` FLOAT(8,2) NOT NULL AFTER `price_dcp`, ADD `margin_dcp` FLOAT(8,2) NOT NULL AFTER `margin_ss`;

ALTER TABLE `tbl_dcp_stock` ADD `cartons_id` INT NOT NULL AFTER `dcp_id`;

ALTER TABLE `tbl_user` ADD `latitude` VARCHAR(255) NOT NULL AFTER `user_status`, ADD `longitude` VARCHAR(255) NOT NULL AFTER `latitude`;

ALTER TABLE `tbl_user` ADD `added_on` DATETIME NOT NULL AFTER `user_status`;

ALTER TABLE `tbl_user` ADD `latitude` VARCHAR(255) NOT NULL AFTER `added_on`, ADD `longitude` VARCHAR(255) NOT NULL AFTER `latitude`;

TRUNCATE TABLE `tbl_dcp_cartons`;
TRUNCATE TABLE `tbl_dcp_cartons_assign`;
TRUNCATE TABLE `tbl_dcp_orders`;
TRUNCATE TABLE `tbl_dcp_stock`;

UPDATE `tbl_user` SET `is_default_user` = '1' WHERE `tbl_user`.`id` = 49000;
UPDATE `tbl_user` SET `is_default_user` = '1' WHERE `tbl_user`.`id` = 49001;

SET time_zone = '+05:30';

TRUNCATE `tbl_brand`;
TRUNCATE `tbl_campaign`;
TRUNCATE `tbl_campaign_area`;
TRUNCATE `tbl_campaign_area_price`;
TRUNCATE `tbl_campaign_product`;
TRUNCATE `tbl_campaign_product_weight`;
TRUNCATE `tbl_category`;
TRUNCATE `tbl_customer_orders`;
TRUNCATE `tbl_dcp_cartons`;
TRUNCATE `tbl_dcp_cartons_assign`;
TRUNCATE `tbl_dcp_orders`;
TRUNCATE `tbl_dcp_stock`;
TRUNCATE `tbl_orders`;
TRUNCATE `tbl_orders_received`;
TRUNCATE `tbl_order_cp_discount`;
TRUNCATE `tbl_order_c_n_f_product`;
TRUNCATE `tbl_order_delivery_flow`;
TRUNCATE `tbl_order_details`;
TRUNCATE `tbl_order_placed`;
TRUNCATE `tbl_order_purchase_p_weight`;
TRUNCATE `tbl_order_stockist`;
TRUNCATE `tbl_product`;
TRUNCATE `tbl_product_variant`;
TRUNCATE `tbl_product_variant_units`;
TRUNCATE `tbl_record_tracker`;
TRUNCATE `tbl_shops`;
TRUNCATE `tbl_shop_assignedto_users`;
TRUNCATE `tbl_shop_payment`;
TRUNCATE `tbl_shop_visit`;
TRUNCATE `tbl_sp_attendance`;
TRUNCATE `tbl_sp_tadabill`;
TRUNCATE `tbl_sp_travel`;
TRUNCATE `tbl_subarea`;
TRUNCATE `tbl_surb`;
TRUNCATE `tbl_user`;
TRUNCATE `tbl_user_details`;
TRUNCATE `tbl_user_location`;
TRUNCATE `tbl_user_working_area`;

INSERT INTO `tbl_clients_maintenance` (`id`, `clientnm`, `paymentmode`, `phone`, `email`, `dateofcontract`, `logo`, `color`, `wsbasepath`, `client_type`) 
VALUES (NULL, 'mesmerik', '1', '12345678', 'paanchocolate@admin.com', '2018-07-12', 'http://salzpoint.net/mesmerik/assets/SiteLogo/paanchocolate_logo.png', '1', 'http://salzpoint.net/mesmerik', '0');

--the table merged are
tbl_salesperson_leave =>tbl_sp_attendance

ALTER TABLE `tbl_product_variant` CHANGE `sgst` `sgst` FLOAT(8,2) NOT NULL;