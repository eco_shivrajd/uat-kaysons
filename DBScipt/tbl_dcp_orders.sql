-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2018 at 04:01 AM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salzpoin_uatkaysons`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dcp_orders`
--

CREATE TABLE `tbl_dcp_orders` (
  `id` int(11) NOT NULL,
  `order_by` int(11) NOT NULL,
  `order_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cartons_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `brand_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_variant_weight1` decimal(10,2) NOT NULL,
  `product_variant_unit1` decimal(10,2) NOT NULL,
  `product_unit_cost` decimal(10,2) NOT NULL,
  `product_total_cost` int(11) NOT NULL,
  `product_cgst` decimal(10,2) NOT NULL,
  `product_sgst` decimal(8,2) NOT NULL,
  `p_cost_cgst_sgst` decimal(10,2) NOT NULL,
  `order_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_dcp_orders`
--
ALTER TABLE `tbl_dcp_orders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_dcp_orders`
--
ALTER TABLE `tbl_dcp_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
