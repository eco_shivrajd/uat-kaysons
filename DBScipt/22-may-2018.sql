
ALTER TABLE `tbl_order_details` ADD `free_product_details` VARCHAR(1000) NOT NULL COMMENT 'brand_id,brand_name,category_id,category_name,product_id,product_name,product_varid,weight,quantity,price' AFTER `campaign_sale_type`;

ALTER TABLE `tbl_shop_visit` ADD `no_o_lat` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `shop_visit_date_time`, ADD `no_o_long` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `no_o_lat`;

ALTER TABLE `tbl_sp_attendance` ADD `todays_travelled_distance` DECIMAL(10,2) NOT NULL AFTER `endlongg`;


CREATE TABLE `tbl_van` (
  `id` int(11) NOT NULL,
  `van_no` varchar(255) NOT NULL,
  `van_rto` varchar(255) NOT NULL,
  `van_type` varchar(255) NOT NULL,
  `van_owner` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `van_cap_wght` varchar(255) NOT NULL,
  `van_cap_boxes` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL,
  `del_status` enum('Loaded','Empty') NOT NULL,
  `added_on_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_van`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_van`
--
ALTER TABLE `tbl_van`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_van`
--
ALTER TABLE `tbl_van`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
  
CREATE TABLE `tbl_language` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lang_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`id`, `name`, `lang_code`) VALUES
(1, 'english', 'en'),
(2, 'telagu', 'te'),
(3, 'tamil', 'ta'),
(4, 'malyalam', 'ml'),
(5, 'hindi', 'hi'),
(6, 'marathi', 'mr'),
(7, 'kannada', 'kn');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_language`
--
ALTER TABLE `tbl_language`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_language`
--
ALTER TABLE `tbl_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
  
  
  
  --
  --Bellow code -04Jun-2018
  --new db for multiple instances changes
  --
  ALTER TABLE `tbl_brand` ADD `added_by_userid` INT NOT NULL AFTER `deleted_on`, ADD `added_by_usertype` VARCHAR(100) NOT NULL AFTER `added_by_userid`;
  
  
ALTER TABLE `tbl_category` ADD `added_by_userid` INT NOT NULL AFTER `deleted_on`, ADD `added_by_usertype` VARCHAR(100) NOT NULL AFTER `added_by_userid`;




ALTER TABLE `tbl_product` ADD `added_by_userid` INT NOT NULL AFTER `deleted_on`, ADD `added_by_usertype` VARCHAR(100) NOT NULL AFTER `added_by_userid`;
  



CREATE TABLE `tbl_order_stockist` (
  `id` int(11) NOT NULL,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordered_by_userid` int(11) DEFAULT NULL,
  `ordered_by_usertype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `categoryname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `producthsn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_variant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `product_cgst` float DEFAULT NULL,
  `product_sgst` float DEFAULT NULL,
  `total_cost` float DEFAULT NULL,
  `discounted_price` float DEFAULT NULL,
  `price_by_weight` float DEFAULT NULL,
  `p_cost_totalwith_tax` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_order_stockist`
--

INSERT INTO `tbl_order_stockist` (`id`, `order_no`, `invoice_no`, `ordered_by_userid`, `ordered_by_usertype`, `order_date`, `categoryname`, `productname`, `producthsn`, `product_variant`, `product_quantity`, `product_cgst`, `product_sgst`, `total_cost`, `discounted_price`, `price_by_weight`, `p_cost_totalwith_tax`) VALUES
(1, 'AB2F2E95', 'AB2F2E95', 1, 'Admin', '2018-06-08 14:52:11', '0', '0', '\r\n                                                ', '0', 2, 0, 0, 0, 0, 0, 244);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_order_stockist`
--
ALTER TABLE `tbl_order_stockist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_order_stockist`
--
ALTER TABLE `tbl_order_stockist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


CREATE TABLE `tbl_customer_new` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_emailid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_phone_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `customer_address` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `customer_type` int(11) NOT NULL,
  `customer_shipping_address` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `subarea` int(11) NOT NULL,
  `addr_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addr_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ship_addr_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ship_addr_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_customer_new`
--
ALTER TABLE `tbl_customer_new`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_customer_new`
--
ALTER TABLE `tbl_customer_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tbl_customer_new` ADD `added_on_date` TIMESTAMP NOT NULL AFTER `ship_addr_long`;

ALTER TABLE `tbl_order_stockist` ADD `product_id` INT NOT NULL AFTER `id`, ADD `product_varient_id` INT NOT NULL AFTER `product_id`;

ALTER TABLE `tbl_shops` ADD `added_on` TIMESTAMP NOT NULL AFTER `shop_status`;

ALTER TABLE `tbl_sp_attendance` ADD `reason` VARCHAR(255) NOT NULL AFTER `todays_travelled_distance`, ADD `description` VARCHAR(255) NOT NULL AFTER `reason`;



CREATE TABLE `tbl_delivery_channel` (
  `id` int(11) NOT NULL,
  `dcp_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dcp_emailid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dcp_phone_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dcp_address` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `dcp_type` int(11) NOT NULL,
  `dcp_shipping_address` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `subarea` int(11) NOT NULL,
  `addr_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addr_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ship_addr_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ship_addr_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_on_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_dcp_new`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_dcp_new`
--
ALTER TABLE `tbl_delivery_channel`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_dcp_new`
--
ALTER TABLE `tbl_delivery_channel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;