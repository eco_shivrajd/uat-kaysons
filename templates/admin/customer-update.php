<?php
include "../includes/header.php";
include "../includes/customerManage.php";
$customerObj 	= 	new customerManager($con,$conmain);
$id=$_GET['id'];
if(isset($_POST['hidbtnsubmit']))
{
    $id =$_POST['id'];
	$customerObj->updateCustomerDetails($id);
	echo '<script>alert("Customer Updated successfully.");</script>';	
}
?>
<!DOCTYPE html>
<!-- BEGIN HEADER -->
 
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Customer";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->			
			<h3 class="page-title">Customer</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="customer.php">Customer</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Customer</a>
					</li>
					
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Customer
							</div>
							<a href="updatecustpassword.php?id=<?php echo $id;?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">										
							<?php
							$id=$_GET['id'];							
							$row1 = $customerObj->getCustomerDetails($id);
							?>                      
                          
							<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action=""> 
								 <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control" value="<?php echo fnStringToHTML($row1['firstname'])?>">
              </div>
            </div><!-- /.form-group -->
             
			<div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
				placeholder="Enter Username"
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-50 characters without blank spaces"
				name="username" class="form-control" value="<?php echo fnStringToHTML($row1['username'])?>"><span id="user-availability-status"></span>
              </div>
            </div>
 <div class="form-group">
			  <label class="col-md-3">Address:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<textarea name="address"
				 placeholder="Enter Address"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"							
				rows="4" class="form-control"><?php echo fnStringToHTML($row1['address'])?></textarea>
			  </div>
			</div><!-- /.form-group -->
			<?php if($page_to_update != 'sales_person'){ ?>
			<div id="working_area_top">		
			<h4>Assign <?php if($page_to_update == 'stockist'){?>Taluka<?php }else{ ?>District<?php }?></h4>		
			</div>
			<div class="form-group">
			  <label class="col-md-3">State:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";
					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state" id="state" class="form-control" 
				data-parsley-trigger="change"
				data-parsley-required="#true" 				
				data-parsley-required-message="Please select State"
				parsley-required="true"
				data-parsley-required-message="Please select State"
				 onChange="fnShowCity(this.value)">
					 <?php
            if($row1['state_ids'] == 0)
              echo "<option value=''>-Select State-</option>";
            while($row_state = mysqli_fetch_array($result_state))
            {
              $selected = "";
              if($row_state['id'] == $row1['state'])
                $selected = "selected";       
            
              echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
            }
          ?>
				</select>
			  </div>
			</div><!-- /.form-group -->

			<div class="form-group" id="city_div">
			  <label class="col-md-3">District:<span class="mandatory">*</span></label>
			  <div class="col-md-4" id="div_select_city">
				 <?php
          $selected = '';
          if($row1['city'] != '')
          {
            $sql_city="SELECT * FROM tbl_city where state_id='".$row1['state']."' ORDER BY name";
            
            $result_city = mysqli_query($con,$sql_city);            
            while($row_city = mysqli_fetch_array($result_city))
            {
              $cat_id=$row_city['id'];
              $selected = "";   
              if($row_city['id'] == $row1['city'])
                $selected = "selected";     
            
              $option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
            }
          }
          else
            $option = "<option value=''>-Select City-</option>";
          
              
        ?>
				<select name="city" id="city" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true"	
				data-parsley-required-message="Please select District"
				><?=$option;?></select>
			  </div>
			</div><!-- /.form-group --> 

		
<div id="working_area_bottom">				
</div>
			<?php } ?>
<div class="form-group">
			  <label class="col-md-3">Email:</label>

			  <div class="col-md-4">
				<input type="text" name="email" id="email" 
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail"		   
				
				data-parsley-trigger="change"
				
				class="form-control" value="<?php echo fnStringToHTML($row1['email'])?>">
			  </div>
			</div><!-- /.form-group -->


			<div class="form-group">
			  <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<input type="text"  name="mobile"
				placeholder="Enter Mobile Number"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-minlength="10"
				data-parsley-minlength-message="Mobile number should be of minimum 10 digits" 
				data-parsley-maxlength="15"				    
				data-parsley-maxlength-message="Only 15 digits are allowed"
				data-parsley-pattern="^(?!\s)[0-9]*$"
				data-parsley-pattern-message="Please enter numbers only"
				class="form-control" value="<?php echo fnStringToHTML($row1['mobile'])?>">
			  </div>
			</div><!-- /.form-group -->
			
				<div class="form-group">
              <label class="col-md-3">GST Number:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter GST Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter GST Number"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed"	
				data-parsley-minlength="15"
				data-parsley-minlength-message="Not Valid GST Number"	
				name="gstnumber" class="form-control" value="<?php echo fnStringToHTML($row1['gst_number_sss'])?>">
              </div>
          </div>


 
             	<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <span class="mandatory">*</span></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"				
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" 
								value="<?php echo fnStringToHTML($row1['accname'])?>"
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"			value="<?php echo fnStringToHTML($row1['accno'])?>"			
							name="accno" class="form-control" >
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Bank Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"			value="<?php echo fnStringToHTML($row1['bank_name'])?>"			
							name="bank_name" class="form-control" >
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"			
							value="<?php echo fnStringToHTML($row1['accbrnm'])?>"			
							name="accbrnm" class="form-control" >
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"			value="<?php echo fnStringToHTML($row1['accifsc'])?>"			
							name="accifsc" class="form-control">
						  </div>
						</div> 
      


								<div class="form-group">
								  <div class="col-md-4 col-md-offset-3">
									<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
									<input type="hidden" name="hidAction" id="hidAction" value="customer-update.php?id=<?=$id;?>">
									<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
									<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button> 
									&nbsp;
									<a href="Customer.php" class="btn btn-primary">Cancel</a>
									<!-- <button name="submit" id="submit" class="btn btn-primary">Submit</button>
									<a href="Customer.php" class="btn btn-primary">Back</a> -->
									<!--<a data-toggle="modal" href="#thankyouModal"  class="btn btn-primary">Delete</a>-->
								  </div>
								</div><!-- /.form-group -->
							   
							  </form>
							  
							  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
								<div class="modal-dialog" style="width:300px;">
									<div class="modal-content">
										<div class="modal-body">
											<p>
											<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
											</p>                     
										  <center><a href="Customer_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
										  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
										  </center>
										</div>    
									</div>
								</div>
							</div>	               
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>

<script>
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'superstockist')
		param = "&nofunction=nofunction";
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory"+param;
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'stockist')
		param = "&nofunction=nofunction";	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
	CallAJAX(url,"div_select_area");
}

 

function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function FnGetSubareaDropDown(id) {
	var suburb_str = $("#area").val();	
	$("#subarea").html('<option value="">-Select-</option>');	
	if(suburb_str != null)	{
		var suburb_arr_count = calculate_data_count(suburb_str);
		if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
			$("#subarea_div").show();	
			var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else if(suburb_arr_count > 1){
			$("#subarea_div").show();	
			var multiple_id = suburb_str.join(", ");
			var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
	}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
}

function checkAvailability() {
	$('#updateform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var id = $("#id").val();
	if(username != '')
	{
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username+'&id='+id,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');					
					return false;
				} else {
					if(email != '')
					{
						jQuery.ajax({
							url: "../includes/checkUserAvailable.php",
							data:'validation_field=email&email='+email+'&id='+id,
							type: "POST",
							async:false,
							success:function(data){			
								if(data=="exist") {
									alert('E-mail already exists.');									
									return false;
								} else {	
									submitFrm();
								}
							},
							error:function (){}
						});
					} else {
						submitFrm();
					}									
				}
			},
			error:function (){}
		});
	}
}
/*
function checkAvailability() {
	var email = $("#email").val();	
	var id = $("#id").val();
	var validate = 0;	
	 
	if(validate == 0)
	{
		var action = $('#hidAction').val();
		$('#updateform').attr('action', action);					
		$('#hidbtnsubmit').val("submit");
		$('#updateform').submit();
	}	
}*/

function submitFrm(){
	var action = $('#hidAction').val();
	$('#updateform').attr('action', action);					
	$('#hidbtnsubmit').val("submit");
	$('#updateform').submit();
}
</script> 
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>