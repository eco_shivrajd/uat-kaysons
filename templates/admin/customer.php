<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}

include "../includes/customerManage.php";
$customerObj 	= 	new customerManager($con,$conmain);
$del_id = $_GET['id'];
if(isset($del_id) && $del_id!='')
{
  $customerObj->deleteCustomerbyid($del_id);
  echo '<script>alert("Customer Deleted Successfully.");location.href="customer.php";</script>';
}
?>

<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Customer";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<h3 class="page-title">
			Customer
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Customer</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Customer Listing
							</div>
                            <a href="customer-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Customer
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									 Name
								</th>								
                                <th>
									 Email
								</th>
                                <th>
									 Mobile Number
								</th>
                                <th>
                                	District
                                </th>
                                <th>
                                	State
                                </th>
								<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") 
								{ ?>
								<th>
                                  Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
							<?php
							$user_type="Superstockist";
							$sql="SELECT * FROM `tbl_customer` where user_type ='Customer' and isdeleted!='1' ";
							$result1 = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result1))
							{
								echo '<tr class="odd gradeX">
								<td>
									 <a href="customer-update.php?id='.$row['id'].'">'.fnStringToHTML($row['firstname']).'</a>
								</td>'; 
                           
								  echo '<td>'.fnStringToHTML($row['email']).'</td>
                                <td>'.fnStringToHTML($row['mobile']).'</td>
                                <td>';
								$city_id=$row['city'];
								if(!empty($city_id)){
									$sql="SELECT name FROM tbl_city where id = $city_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									}
								}else{
									echo '-';
								}
									
								echo '</td><td>';
								$state_id=$row['state'];
								if(!empty($state_id)){
									$sql="SELECT name FROM tbl_state where id = $state_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									}									
								}else{
									echo '-';
								}
								echo '</td>';
								if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {
									echo '<td>
									 <a href="customer.php?id='.$row['id'].'">Delete</a>
								</td>'; 
								}
								echo'</tr>';	
							} ?> 
							
							</tbody>
							</table>
						</div>
					</div>                   
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>