<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);
$cartonid = $_POST['cartonid'];
$dcpid = $_POST['dcpid'];


$order_type = 'Carton Details'; //$_POST['order_type'];
$order_details = $reportObj->getDCPStockDetails($cartonid, $dcpid); //
//echo "<pre>";print_R($order_details);die();
//$product_variant = $reportObj->getSProductVariant($order_details['product_variant_id']);
?>
<div class="modal-header">
    <button type="button" name="btnPrint" id="btnPrint" onclick="OrderDetailsPrint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
    <?php if ($order_details[0]['lat'] != "" && $order_details[0]['long'] != "" && $order_details[0]['lat'] != "0.0" && $order_details[0]['long'] != "0.0") { ?>
        &nbsp;&nbsp;
        <button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?= $order_details[0]['lat']; ?>', '<?= $order_details[0]['long']; ?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
    <?php } ?>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
    <div class="row">
        <div class="col-md-12">   
            <div class="portlet box blue-steel">
                <div class="portlet-title ">
                    <div class="caption printHeading">
                        <?= $order_type; ?>
                    </div>                          
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
                        <tr>
                            <td>Carton Received By</td>
                            <td><?php echo $order_details[0]['firstname']; ?></td>				
                        </tr>
                        <tr>
                            <td>Carton Name</td>
                            <td><?= "carton-" . $order_details[0]['cartons_id']; ?></td>				
                        </tr>

                        <tr>
                            <td>Cartons Received Details</td>
                            <td>
                                <table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
                                    <thead>
                                        <tr>
                                            <th data-filterable="false" data-sortable="true" data-direction="desc">Sr NO.</th>
                                            <th data-filterable="false" data-sortable="true" data-direction="desc">Product Name</th> 
                                            <th data-filterable="false" data-sortable="true" data-direction="desc">Product variant</th> 
                                            <th data-filterable="false" data-sortable="true" data-direction="desc">Assigned Quantity</th>   
                                            <th data-filterable="false" data-sortable="false" data-direction="desc">Stock Quantity</th>	
                                            <th data-filterable="false" data-sortable="false" data-direction="desc">Sale Quantity</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                    </tbody>
                                    <?php $i=1; foreach ($order_details as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <?php
                                            $product_name = '';
                                            $product_name = $value['cat_name'] . "" . $value['product_name'];
                                            $product_variant1 = $value['product_variant1'] ;
                                            $product_variant2 = $value['product_variant2'];
                                           
                                            ?>
                                            <td><?= $product_name; ?></td> 
                                            <td > <?php
                                                        $imp_variant1 = explode(',', $product_variant1);
                                                        $var_unit_id = trim($product_variant1, ",");
                                                        $sqlvarunit = "SELECT unitname FROM tbl_units  where id='" . $imp_variant1[1] . "'";
                                                        $resultvarunit = mysqli_query($con, $sqlvarunit);
                                                        while ($rowvarunit = mysqli_fetch_array($resultvarunit)) {
                                                            if ($imp_variant1[0] != '' ) {
                                                                echo $imp_variant1[0] . "-" . $rowvarunit['unitname'] . " ";
                                                            }
                                                        }
                                                        $imp_variant2 = explode(',', $product_variant2);
                                                        //echo "123  <pre>";print_r($imp_variant1);
                                                        $var_unit_id = trim($product_variant2, ",");
                                                        $sqlvarunit = "SELECT unitname FROM tbl_units  where id='" . $imp_variant2[1] . "'";
                                                        $resultvarunit = mysqli_query($con, $sqlvarunit);
                                                        while ($rowvarunit = mysqli_fetch_array($resultvarunit)) {
                                                            if ($imp_variant2[0] != '' ) {
                                                                echo $imp_variant2[0] . "-" . $rowvarunit['unitname'] . "  ";
                                                            } else {
                                                                echo "-" . $rowvarunit['unitname'] . "  ";
                                                            }
                                                        }
                                                        echo "<br>";
                                                        ?>
                                                </td> 
                                            <td ><?php echo $value['assigned_qnty']; ?></td>   
                                            <td ><?php echo $value['stock_qnty']; ?></td>	
                                            <td><?php echo $value['sale_qnty']; ?></td>
                                        </tr>
                                    <?php $i++;} ?>

                                </table>
                            </td>				
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>