<!-- BEGIN HEADER -->
<?php
include "../includes/header.php";
include "../includes/userManage.php";
$userObj = new userManager($con, $conmain);
if (isset($_POST['hidbtnsubmit'])) {
    //print"<pre>";print_r($_POST);
    $user_type = "DeliveryChannelPerson";
    /* Local user section */
    $userid_local = $userObj->addLocalUserDetails($user_type);
    $userObj->addLocalUserWorkingAreaDetails($userid_local);
    $userid_local1 = $userObj->addLocalUserOtherDetails($userid_local); //

    /* Common user section */
    $username = fnEncodeString($_POST['username']);
    $common_user_record = $userObj->getCommonUserDetailsByUsername($username);
    if ($common_user_record != 0) {
        $common_user_company_record = $userObj->getCommonUserCompanyDetails($userid_local);
        if ($common_user_company_record != 0) {
            $userObj -> addCommonUserCompanyDetails($userid_local);
        }
    } else {
        $userid_common = $userObj->addCommonUserDetails($user_type, $userid_local);
        $userObj->addCommonUserCompanyDetails($userid_local);
    }
    $email = fnEncodeString($_POST['email']);
    if ($email != '')
        $userObj->sendUserCreationEmail();

    echo '<script>alert("DeliveryChannel  person added successfully.");location.href="dcpusers.php";</script>';
}
?>
    <!-- END HEADER -->

    <body class="page-header-fixed page-quick-sidebar-over-content ">
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "DeliveryChannel";
        include "../includes/sidebar.php";
        ?>

                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <h3 class="page-title">Delivery Channel Person</h3>
                        <div class="page-bar">
                            <ul class="page-breadcrumb">

                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="dcpusers.php">Delivery Channel Person</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Add New Delivery Channel Person</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN PAGE CONTENT-->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Begin: life time stats -->
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption">Add New Delivery Channel Person</div>
                                    </div>
                                    <div class="portlet-body">
                                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                                        <form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">
                                            
                                            <div class="form-group">
                                                <label class="col-md-3">Superstockist:<span class="mandatory">*</span></label>
                                                <div class="col-md-4">
                                                        <select name="cmbSuperStockist" id="cmbSuperStockist"
                                                        class="form-control">

                                                        <?php
                                                        $user_type1="Superstockist";
                                                        $result1 = $userObj->getLocalUserDetailsByUserType($user_type1);
                                                        $i =1;								
                                                        while($row = mysqli_fetch_array($result1))
                                                        {
                                                                $cat_id=$row['id'];
                                                                if($i==1)
                                                                        $ss_id=$row['id'];
                                                                echo "<option value='$cat_id'>" . fnStringToHTML($row['firstname']) . "</option>";
                                                                $i++;
                                                        }
                                                        ?>
                                                        </select>
                                                </div>
                                        </div><!-- /.form-group -->  
                                            <!--new code start-->
                                            <div class="form-group">
                                                <label class="col-md-3">Name:<span class="mandatory">*</span></label>
                                                <div class="col-md-4">
                                                    <input type="text" placeholder="Enter Name" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please enter name" data-parsley-maxlength="50" data-parsley-maxlength-message="Only 50 characters are allowed" name="firstname" class="form-control">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                                <label class="col-md-3">Username:<span class="mandatory">*</span></label>

                                                <div class="col-md-4">
                                                    <input type="text" id="username" placeholder="Enter Username" data-parsley-minlength="6" data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces" data-parsley-maxlength="50" data-parsley-maxlength-message="Only 50 characters are allowed" data-parsley-type-message="Please enter Username, blank spaces are not allowed" data-parsley-required-message="Please enter Username, blank spaces are not allowed" data-parsley-trigger="change" data-parsley-required="true" data-parsley-pattern="/^\S*$/" data-parsley-error-message="Username should be 6-50 characters without blank spaces" name="username" class="form-control"><span id="user-availability-status"></span>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Password:<span class="mandatory">*</span></label>

                                                <div class="col-md-4">
                                                    <input type="password" id="password" placeholder="Enter Password" data-parsley-minlength="6" data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces" data-parsley-maxlength="50" data-parsley-maxlength-message="Only 50 characters are allowed" data-parsley-type-message="Password should be minimum 6 characters without blank spaces" data-parsley-required-message="Please enter Password" data-parsley-trigger="change" data-parsley-required="true" data-parsley-pattern="/^\S*$/" data-parsley-error-message="Password should be 6-50 characters without blank spaces" name="password" class="form-control placeholder-no-fix">
                                                    <!--<span toggle="#password" id="toggle-password" class="fa fa-fw fa-eye field-icon toggle-password"></span>-->
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Confirm Password:<span class="mandatory">*</span></label>
                                                <div class="col-md-4">
                                                    <input type="password" id="c_password" placeholder="Enter Confirm Password" data-parsley-trigger="change" data-parsley-equalto="#password" data-parsley-equalto-message="Password does not match with confirm password" data-parsley-required="#true" data-parsley-required-message="Please enter confirm password" name="c_password" class="form-control"><span id="user-availability-status"></span>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Address:</label>
                                                <div class="col-md-4">
                                                    <textarea name="address" placeholder="Enter Address" data-parsley-trigger="change" data-parsley-maxlength="200" data-parsley-maxlength-message="Only 200 characters are allowed" rows="4" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Email:</label>

                                                <div class="col-md-4">
                                                    <input type="text" id="email" placeholder="Enter E-mail" data-parsley-maxlength="100" data-parsley-maxlength-message="Only 100 characters are allowed" data-parsley-type="email" data-parsley-type-message="Please enter valid e-mail" data-parsley-trigger="change" name="email" class="form-control"><span id="user-availability-status"></span>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Mobile Number:</label>

                                                <div class="col-md-4">
                                                    <input type="text" name="mobile" placeholder="Enter Mobile Number" data-parsley-trigger="change" data-parsley-minlength="10" data-parsley-maxlength="15" data-parsley-maxlength-message="Only 15 characters are allowed" data-parsley-pattern="^(?!\s)[0-9]*$" data-parsley-pattern-message="Please enter numbers only" class="form-control" value="">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                                <label class="col-md-3">State:</label>
                                                <div class="col-md-4">
                                                    <select name="state" id="state" class="form-control" onChange="fnShowCity(this.value)">
                                                        <option selected disabled>-select-</option>
                                                        <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    $selected_state = '';
                                                    if ($default_state == $row['id'])
                                                        $selected_state = 'selected';
                                                    echo "<option value='$cat_id' $selected_state>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group" id="city_div" style="display:none;">
                                                <label class="col-md-3">District:</label>
                                                <div class="col-md-4" id="div_select_city">
                                                    <select name="city" id="city" class="form-control" onchange="FnGetSuburbDropDown(this)">
                                                        <?php
                                                        $sql = "SELECT id,name FROM tbl_city where state_id=" . $default_state . " order by name";
                                                        $result = mysqli_query($con, $sql);
                                                        while ($row = mysqli_fetch_array($result)) {
                                                            $cat_id = $row['id'];
                                                            $selected_city = '';
                                                            if ($default_city == $row['id'])
                                                                $selected_city = 'selected';
                                                            echo "<option value='$cat_id' $selected_city>" . $row['name'] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group" id="area_div" style="display:none;">
                                                <label class="col-md-3">Taluka:</label>
                                                <div class="col-md-4" id="div_select_area">

                                                    <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                        <?php
                                                $sql = "SELECT id,suburbnm FROM tbl_surb where cityid=$default_city order by suburbnm";
                                                $result = mysqli_query($con, $sql);
                                                $row_count = mysqli_num_rows($result);
                                                if ($row_count > 0) {
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        $cat_id = $row['id'];
                                                        $selected_area = '';
                                                        if ($default_area == $row['id'])
                                                            $selected_area = 'selected';
                                                        echo "<option value='$cat_id' $selected_area>" . $row['suburbnm'] . "</option>";
                                                    }
                                                }else {
                                                    echo "<option selected value=''>-Select-</option>";
                                                }
                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group" id="subarea_div" style="display:none;">
                                                <label class="col-md-3">Subarea:</label>
                                                <div class="col-md-4" id="div_select_subarea">
                                                    <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
                                                        <option selected value="">-Select-</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->

                                            <!--new code end -->
                                            <div class="form-group">
                                                <label class="col-md-3">Status:</label>
                                                <div class="col-md-4">

                                                    <select name="user_status" id="user_status" class="form-control">
                                                        <option value="Active" selected>Active</option>
                                                        <option value="Inactive">Inactive</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <!--new code end-->

                                            <div class="form-group">
                                                <div class="col-md-4 col-md-offset-3">
                                                    <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                                                    <input type="hidden" name="hidAction" id="hidAction" value="dcpuser-add.php">
                                                    <button type="button" name="btnsubmit" onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
                                                    <a href="dcpusers.php" class="btn btn-primary">Cancel</a>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                        </form>
                                    </div>
                                </div>
                                <!-- End: life time stats -->
                            </div>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->

                <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include "../includes/footer.php" ?>
            <script>
                function calculate_data_count(element_value) {
                    element_value = element_value.toString();
                    var element_arr = element_value.split(',');
                    return element_arr.length;
                }

                function setSelectNoValue(div, select_element) {
                    var select_selement_section = '<select name="' + select_element + '" id="' + select_element + '" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
                    document.getElementById(div).innerHTML = select_selement_section;
                }

                function fnShowCity(id_value) {
                    $("#city_div").show();
                    var param='';
                    $("#area").html('<option value="">-Select-</option>');
                    $("#subarea").html('<option value="">-Select-</option>');
                    var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory" + param;
                    CallAJAX(url, "div_select_city");
                }

                function FnGetSuburbDropDown(id) {
                    $("#area_div").show();
                    $("#subarea").html('<option value="">-Select-</option>');
                    var param = '';
                    var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown" + param;
                    CallAJAX(url, "div_select_area");
                }

                function FnGetSubareaDropDown(id) {
                    $("#subarea_div").show();
                    var url = "getSubareaDropdown.php?area_id=" + id.value + "&select_name_id=subarea";
                    CallAJAX(url, "div_select_subarea");
                }

                function checkAvailability() {
                    $('#addform').parsley().validate();
                    var email = $("#email").val();
                    var username = $("#username").val();
                    var validate = 1;
                    if (username != '') {
                        validate = 1;
                        jQuery.ajax({
                            url: "../includes/checkUserAvailable.php",
                            data: 'validation_field=username&username=' + username,
                            type: "POST",
                            async: false,
                            success: function(data) {
                                if (data == "exist") {
                                    alert('Username already exists.');
                                    validate = 1;
                                } else {
                                    validate = 0;
                                }
                            },
                            error: function() {}
                        });
                    }

                    if (email != '') {
                        validate = 1;
                        jQuery.ajax({
                            url: "../includes/checkUserAvailable.php",
                            data: 'validation_field=email&email=' + email,
                            type: "POST",
                            async: false,
                            success: function(data) {
                                if (data == "exist") {
                                    alert('E-mail already exists.');
                                    validate = 1;
                                    return false;
                                } else {
                                    validate = 0;
                                }
                            },
                            error: function() {}
                        });
                    } else if (username != '' && validate == 0)
                        validate = 0;
                    else
                        validate = 1;

                    if (validate == 0) {
                        var action = $('#hidAction').val();
                        $('#addform').attr('action', action);
                        $('#hidbtnsubmit').val("submit");
                        $('#addform').submit();
                    }
                }

                /*$("#toggle-password").click(function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                	var input = $($(this).attr("toggle"));
                	if (input.attr("type") == "password") {
                		input.attr("type", "text");
                	} else {
                		input.attr("type", "password");
                	}
                });*/
            </script>
            <style>
                .field-icon {
                    float: right;
                    margin-right: 10px;
                    margin-top: -30px;
                    position: relative;
                    z-index: 2;
                }
            </style>
            <!-- END FOOTER -->
            <style>
                .form-horizontal {
                    font-weight: normal;
                }
            </style>
    </body>
    <!-- END BODY -->

    </html>