<!-- BEGIN HEADER -->
<?php
include "../includes/header.php";
include "../includes/userManage.php";
$userObj = new userManager($con, $conmain);
?>
<!-- END HEADER -->
<?php
if (isset($_POST['hidbtnsubmit'])) {
    $id = $_POST['id'];
    $userObj->updateCommonDeliveryChannelDetails($id);  
    echo '<script>location.href="delivery_channel.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "DeliveryChannel";
        include "../includes/sidebar.php"
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <!-- /.modal -->
                <h3 class="page-title">
                    Stockist
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="delivery_channel.php">Delivery Channel</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"><? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { echo "Edit Stockist"; } else { echo "Stockist Details"; } ?></a> 
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Begin: life time stats -->
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">                                  
                                </div>
                                <a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']); ?>" class="btn btn-sm btn-default pull-right mt5">							
                                    Change Password
                                </a>
                            </div>
                            <div class="portlet-body">
                                <? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
                                <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                                <?}?>
                                <?php
                                $id = $_GET['id'];                               
                                $userObj = new userManager($con, $conmain);
                                $user_details = $userObj->getCommonDeliveryChannelDetails($id);                               
                                ?>                       
                                <form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">

                                    <?php $page_to_update = 'stockistlike';
                                    include "userUpdateCommEle.php"; //form common element file with javascript validation 
                                    ?>             

                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                                            <input type="hidden" name="hidAction" id="hidAction" value="delivery_channel_edit.php">
                                            <input type="hidden" name="id" id="id" value="<?= $row1['id']; ?>">
                                            <button type="button"  name="btnsubmit"  onclick="return checkAvailability_for_delivery_channel();" class="btn btn-primary">Submit</button>
                                            <a href="delivery_channel.php" class="btn btn-primary">Cancel</a>  
                                        </div>
                                    </div><!-- /.form-group -->

                                </form>

                            </div>
                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <script>
        function checkAvailability_for_delivery_channel() {
            $('#addform').parsley().validate();
            var email = $("#email").val();
            var username = $("#username").val();
            var validate = 1;
            if (username != '')
            {
                validate = 1;
                jQuery.ajax({
                    url: "../includes/checkDeliveryChannelAvailable.php",
                    data: 'validation_field=username&username=' + username,
                    type: "POST",
                    async: false,
                    success: function (data) {
                        if (data == "exist") {
                            alert('Username already exists.');
                            validate = 1;
                        } else {
                            validate = 0;
                        }
                    },
                    error: function () {}
                });
            }

            if (email != '')
            {
                validate = 1;
                jQuery.ajax({
                    url: "../includes/checkDeliveryChannelAvailable.php",
                    data: 'validation_field=email&email=' + email,
                    type: "POST",
                    async: false,
                    success: function (data) {
                        if (data == "exist") {
                            alert('E-mail already exists.');
                            validate = 1;
                            return false;
                        } else {
                            validate = 0;
                        }
                    },
                    error: function () {}
                });
            } else if (username != '' && validate == 0)
                validate = 0;
            else
                validate = 1;

            if (validate == 0)
            {
                var action = $('#hidAction').val();
                $('#addform').attr('action', action);
                $('#hidbtnsubmit').val("submit");
                $('#addform').submit();
            }
        }
        function showStateCity(obj) {
            var ss_id = obj.value;
            jQuery.ajax({
                url: "../includes/ajax_getUserAssLocation.php",
                data: 'ss_id=' + ss_id,
                type: "POST",
                async: false,
                success: function (data) {
                    var user_record = JSON.parse(data);
                    if (user_record != 0) {
                        var state = user_record.state;
                        var city = user_record.city;
                        $("#state").val(state);
                        setTimeout(
                                function ()
                                {
                                    fnShowCity(state);
                                    setTimeout(
                                            function ()
                                            {
                                                $("#city").val(city);
                                            }, 1800);
                                }, 600);
                        setTimeout(function ()
                        {
                            FnGetSuburbDropDown($("#city").get(0));
                            setTimeout(
                                    function ()
                                    {
                                        var selected_taluka = $("#selected_taluka").val();
                                        $("#area").val(selected_taluka);
                                    }, 3800);
                        }, 2800);
                    }
                },
                error: function () {}
            });
        }
    </script>
    <!-- BEGIN FOOTER -->
<?php include "../includes/footer.php" ?>
    <!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>