<?php include "../includes/header.php";
setlocale(LC_MONETARY, 'en_IN');
?>
<!-- END HEADER -->
<script type="text/javascript">
function fnSelectionBoxTest()
{
	var str = $( "form" ).serialize();	
	document.getElementById('hdnSelrange').value=document.getElementById('selTest').value;
	if(document.getElementById('selTest').value == '3')
	{
	   document.getElementById('date-show').style.display = "block";
	}
	else
	document.frmSearch.submit();
}
 
</script>
<?php

switch($_SESSION[SESSION_PREFIX.'user_type']){
	case "Admin":
		//get Sales count for this month
		/*$sqltotalmonthsale	=	"SELECT SUM(od.p_cost_cgst_sgst) as total_amt FROM `tbl_order_details` AS VO,tbl_orders WHERE tbl_orders.id=VO.order_id AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '') AND date_format(order_date, '%m')=date_format(now(), '%m')";*/
		  $sqltotalmonthsale="SELECT SUM(od.p_cost_cgst_sgst) FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE  date_format(o.order_date, '%m')=date_format(now(), '%m')";
		$resultmonthsale 	=	mysqli_query($con,$sqltotalmonthsale);
		$rowmonthsale 		=	mysqli_fetch_array($resultmonthsale);
		//end
		//get Sales count for all
		/*$sqltotaltsale="SELECT SUM(od.p_cost_cgst_sgst) as total_amt FROM `tbl_order_details` AS VO WHERE  (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'   OR VO.campaign_sale_type LIKE '' ) ";*/
		$sqltotaltsale="SELECT SUM(od.p_cost_cgst_sgst) FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id";
		$resulttotaltsale = mysqli_query($con,$sqltotaltsale);
		$rowtotaltsale = mysqli_fetch_array($resulttotaltsale);
		//get new sales
		//get Sales count for all
		 $sqlnewsale="SELECT SUM(od.p_cost_cgst_sgst) FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE  date_format(o.order_date, '%Y-%m-%d')=date_format(now(), '%Y-%m-%d')";

		$resultnewsale = mysqli_query($con,$sqlnewsale);
		$rownewsale = mysqli_fetch_array($resultnewsale);
	break;
	case "Superstockist":
		//get Sales count for this month
		$sqltotalmonthsale="SELECT SUM(od.p_cost_cgst_sgst) FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE date_format(o.order_date, '%m')=date_format(now(), '%m')  AND (od.campaign_sale_type IS NULL OR od.campaign_sale_type = 'sale'   OR od.campaign_sale_type LIKE '' ) AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'"; //AND (VO.status='2' OR VO.status='3')
		$resultmonthsale = mysqli_query($con,$sqltotalmonthsale);
		$rowmonthsale = mysqli_fetch_array($resultmonthsale);
		//end
		//get Sales count for all
		$sqltotaltsale="SELECT SUM(od.p_cost_cgst_sgst) FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE  (od.campaign_sale_type IS NULL OR od.campaign_sale_type = 'sale' OR od.campaign_sale_type LIKE '' )  AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'"; //(VO.status='2' OR VO.status='3') AND
		$resulttotaltsale = mysqli_query($con,$sqltotaltsale);
		$rowtotaltsale = mysqli_fetch_array($resulttotaltsale);
		//get new sales
		//get Sales count for all
		 $sqlnewsale="SELECT SUM(od.p_cost_cgst_sgst) FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE date_format(o.order_date, '%Y-%m-%d')=date_format(now(), '%Y-%m-%d') AND  (od.campaign_sale_type IS NULL OR od.campaign_sale_type = 'sale'  OR od.campaign_sale_type LIKE '' )  AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'";//AND VO.status='2' AND
		$resultnewsale = mysqli_query($con,$sqlnewsale);
		$rownewsale = mysqli_fetch_array($resultnewsale);
	break;
	case "Distributor":
		//get Sales count for this month
		$sqltotalmonthsale="SELECT SUM(VO.p_cost_cgst_sgst) as total_amt FROM `tbl_order_details` AS VO,tbl_orders WHERE tbl_orders.id=VO.order_id AND date_format(order_date, '%m')=date_format(now(), '%m') AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '' )  AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resultmonthsale = mysqli_query($con,$sqltotalmonthsale);
		$rowmonthsale = mysqli_fetch_array($resultmonthsale);
		//end
		//get Sales count for all
		$sqltotaltsale="SELECT SUM(VO.p_cost_cgst_sgst) as total_amt FROM `tbl_order_details` AS VO,tbl_orders WHERE tbl_orders.id=VO.order_id AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '')  AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resulttotaltsale = mysqli_query($con,$sqltotaltsale);
		$rowtotaltsale = mysqli_fetch_array($resulttotaltsale);
		//get new sales
		//get Sales count for all
		$sqlnewsale="SELECT SUM(VO.p_cost_cgst_sgst) as total_amt FROM `tbl_order_details` AS VO,tbl_orders WHERE tbl_orders.id=VO.order_id AND VO.status='1' AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '')  AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resultnewsale = mysqli_query($con,$sqlnewsale);
		$rownewsale = mysqli_fetch_array($resultnewsale);
	break;
}

?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

<?php 
$activeMainMenu = "Dashboard"; $activeMenu = "";
include "../includes/sidebar.php"; 
?>

<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Dashboard <small>Reports & Statistics</small></h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Dashboard</a>
					</li>					
				</ul>
				<!--<a id="treeview" href="#" class="btn btn-sm btn-default pull-right mt5">
                                Treeview
				</a>-->
			</div>
			<!-- END PAGE HEADER-->		
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-comments"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php
									$monthsale=$rowmonthsale[0];
									 $monthsale = money_format('%!i', $monthsale);
								 echo $monthsale;
								 //echo number_format($rowmonthsale[0],2);
								 ?>&nbsp;<i aria-hidden="true" class="fa fa-inr fa-6"></i>
							</div>
							<div class="desc">Monthly Sales</div>
						</div>						
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
								  <?php 
								  $amounttotal=$rowtotaltsale[0];
								  $amounttotal = money_format('%!i', $amounttotal);
								  echo $amounttotal;
								 // echo number_format($rowtotaltsale[0],2);
								  ?>&nbsp;<i aria-hidden="true" class="fa fa-inr fa-6"></i>
							</div>
							<div class="desc">
								 All Brand And All Categories Total Sales
							</div>
						</div>						
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<i class="fa fa-shopping-cart"></i>
						</div>
						<div class="details">
							<div class="number">
							<?php
							$todaysaleamount = $rownewsale[0];							
							//$amount = money_format('%!i', $rownewsale[0]);
							$todaysaleamount = money_format('%!i', $todaysaleamount);
							echo $todaysaleamount;?>&nbsp;<i aria-hidden="true" class="fa fa-inr fa-6"></i>
							</div>
							<div class="desc">
								 Todays Sales
							</div>
						</div>						
					</div>
				</div>
			</div>
			
			<!-- END DASHBOARD STATS -->
			<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal" name="frmSearch" id="frmSearch" method="post">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-1 control-label">Options:</label>
					<div class="col-sm-2">						
						<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
						<option value='0'>-Select-</option>
						<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
						<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
						<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
						<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
						</select>
						<input type="hidden" name="hdnSelrange" id="hdnSelrange">
					</div>
<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
<label for="inputEmail3" class="col-sm-2 control-label">Order Status:</label>
<div class="col-sm-2">                                                                                   
	<select class="form-control" name="order_status" id="order_status" onChange="fnSelectionBoxTest()">
	<option value="">-Select-</option>
	<option value="1" <?php if($_REQUEST['order_status']=="1")echo 'selected';?>>Received</option>             
	<option value="2" <?php if($_REQUEST['order_status']=="2")echo 'selected';?>>Assigned for Delivery</option>
<!-- 	<option value="3" <?php if($_REQUEST['order_status']=="3")echo 'selected';?>>Assigned to Transport office </option>  -->                                                                                                                           
	<option value="4" <?php if($_REQUEST['order_status']=="4")echo 'selected';?>>Delivered</option>
	<option value="6" <?php if($_REQUEST['order_status']=="6")echo 'selected';?>>Payment Received</option>
	<option value="8" <?php if($_REQUEST['order_status']=="8")echo 'selected';?>>Partial Payment Received</option>  
<?php } ?>
	<input type="hidden" name="hdnOrderstatus" id="hdnOrderstatus">                                                
	</select>
</div>
				</div>
				<div class="form-group">
					<?php 
					if($_REQUEST['selTest']=="3"){
						$dtdisp="display:block;";
						$frmdate = $_REQUEST['frmdate'];
						$todate = $_REQUEST['todate'];
					}
					else
					{
						$dtdisp="display:none;";
						$frmdate = "";
						$todate = "";
					} ?>
					<div id="date-show" style="<?php echo $dtdisp;?>">
						<label for="inputEmail3" class="col-sm-1 control-label">From Date:</label>
						<div class="col-md-2">
							<div class="input-group  date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
							<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly>
							<span class="input-group-btn">
							<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<!-- /input-group -->
						</div>
						<label for="inputEmail3" class="col-sm-1 control-label">To Date:</label>
						 
							<div class="col-md-2">
								<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
								<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly>
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							</div>
							<div class="col-sm-2">
								<button type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-primary">Submit</button>
								<!-- /input-group -->
							</div>
						 
					</div>
				</form> 
				
				 <div class="clearfix"></div>
				 
				<div class="col-md-12">&nbsp;</div>	
                
                <div class="clearfix"></div>
				
				
				<div class="col-md-4">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Category Wise Trend 
							</div>
						
						</div>
						<div class="portlet-body">
							<div id="chartdiv2" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				
			  <!--   <? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?> -->
				<div class="col-sm-4">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Product Wise Trend
							</div>
						
						</div>
						<div class="portlet-body">
							<div id="chartdiv3" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<!-- <? } ?> -->

				 <? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
				<div class="col-sm-4">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Region Wise Trend
							</div>
						
						</div>
						<div class="portlet-body">
							<div id="chartdiv" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<? } ?>
				<div class="col-md-4">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Sales Person Leave 
							</div>						
						</div>
						<div class="portlet-body">
							<div id="chartleave" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				
			
				<div class="col-sm-8">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Sales Person Current Location
							</div>
						
						</div>
						<div class="portlet-body">
							<div id="chartsp_current" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				
				<div class="clearfix"></div>
			</div>
		
		</div>
		
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="portlet box blue-steel">
				<div class="portlet-title">
					<div class="caption"><i class="icon-puzzle"></i>Shop Added Summary Report</div>
					<button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
					&nbsp;
					<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
				</div>	
				<div class="portlet-body" id="order_summary_details">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
	</div>
	<!-- END CONTENT -->
	<!--Treeview Modal -->
 
	<div id="dialog" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			  </div>
			  <div class="modal-body">
				<p>Some text in the modal.</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
	  </div>
<!--Treeview modal end-->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<script>

            $(document).ready(function () {
                ShowReport(1);
                $.ajax
                ({
                    type: "POST",
                    url: "sales_leave_details.php",
                    success: function (msg)
                    {
                        $("#chartleave").html(msg);
                    }
                });
                 $.ajax
                ({
                    type: "POST",
                    url: "sales_location_current.php",
                    success: function (msg)
                    {
                        $("#chartsp_current").html(msg);
                    }
                });
            });
           
            function ShowReport(page) {
                //alert("dsfsd");
                var param = '';
				var date = new Date();
				date.setDate(date.getDate() - 1);
               param = param + 'frmdate=' + date;
				$.ajax
				({
					type: "POST",
					url: "dashboard_sales_expenses.php",
					data: param,
					success: function (msg)
					{
						$("#order_summary_details").html(msg);
					}
				});
                
            }

            function report_download() {
                var td_rec = $("#sample_2 td:last").html();
                if (td_rec != 'No matching records found')
                {
                    var divContents = $(".table-striped").html();
                    $("#print_div").html('<table id="print_table" style="text-decoration:none;">' + divContents + '</table>');
                    var heading = $("#table_heading").html();
                    $("#print_table tr th i").html("RS");
                    divContents = $("#print_div").html();

                    divContents = divContents.replace(/<\/*a.*?>/gi, '');
                    $("#export_data").val(divContents);
                    document.forms.export_excel.submit();
                } else {
                    alert("No matching records found");
                }
            }

            $("#btnPrint").live("click", function () {
                var td_rec = $("#sample_2 td:last").html();
                if (td_rec != 'No matching records found')
                {
                    var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);

                    var divContents1 = $(".table-scrollable").html();
                    $("#print_div").html(divContents1);
                    $("#print_div a").removeAttr('href');
                    $("#sample_2 tr th i").html("");
                    var divContents = $("#print_div").html();
                    var printWindow = window.open('', '', 'height=400,width=800');
                    printWindow.document.write('<html><head><title>Report</title>');
                    printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
                    printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
                    printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');

                    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                        printWindow.document.write('</head><body >');
                        printWindow.document.write(divContents);
                        printWindow.document.write('</body></html>');
                        printWindow.focus();
                        setTimeout(function () {
                            printWindow.print();
                            //printWindow.close();
                        }, 500);
                    } else if (isIE == true) {
                        printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
                        printWindow.document.write('</head><body >');
                        printWindow.document.write(divContents);
                        printWindow.document.write('</body></html>');
                        printWindow.focus();
                        printWindow.document.execCommand("print", false, null);
                        //printWindow.close();
                    } else {
                        printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
                        printWindow.document.write('</head><body >');
                        printWindow.document.write(divContents);
                        printWindow.document.write('</body></html>');
                        printWindow.focus();
                        setTimeout(function () {
                            printWindow.print();
                            //printWindow.close();
                        }, 100);
                    }
                } else {
                    alert("No matching records found");
                }
            });
        </script>
</body>
<!-- END BODY -->
</html>