<!-- BEGIN HEADER -->
<?php
include "../includes/header.php";
include "../includes/userManage.php";
$userObj = new userManager($con, $conmain);
if (isset($_POST['hidbtnsubmit'])) {
    $userid_common = $userObj->addCommonDeliveryChannelDetails();
    $email = fnEncodeString($_POST['email']);
    if ($email != '')
        $userObj->sendUserCreationEmail();
    echo '<script>alert("Delivery Channel added successfully.");location.href="delivery_channel.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "DeliveryChannel";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Delivery Channel
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="delivery_channel.php">Delivery Channel</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Add New Delivery Channel</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Begin: life time stats -->
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Add New Delivery Channel
                                </div>

                            </div>
                            <div class="portlet-body">
                                <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                       


                                <form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">



                                    <?php
                                    $page_to_add = 'stockistlike';
                                    include "userAddCommEle.php"; //form common element file with javascript validation  
                                    ?>    

                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">							
                                            <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                                            <input type="hidden" name="hidAction" id="hidAction" value="delivery_channel_add.php">
                                            <button type="button"  name="btnsubmit"  onclick="return checkAvailability_for_delivery_channel();" class="btn btn-primary">Submit</button>
                                            <a href="delivery_channel.php" class="btn btn-primary">Cancel</a>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>                              
                            </div>
                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <script>
        function checkAvailability_for_delivery_channel() {
            $('#addform').parsley().validate();
            var email = $("#email").val();
            var username = $("#username").val();
            var validate = 1;
            if (username != '')
            {
                validate = 1;
                jQuery.ajax({
                    url: "../includes/checkDeliveryChannelAvailable.php",
                    data: 'validation_field=username&username=' + username,
                    type: "POST",
                    async: false,
                    success: function (data) {
                        if (data == "exist") {
                            alert('Username already exists.');
                            validate = 1;
                        } else {
                            validate = 0;
                        }
                    },
                    error: function () {}
                });
            }

            if (email != '')
            {
                validate = 1;
                jQuery.ajax({
                    url: "../includes/checkDeliveryChannelAvailable.php",
                    data: 'validation_field=email&email=' + email,
                    type: "POST",
                    async: false,
                    success: function (data) {
                        if (data == "exist") {
                            alert('E-mail already exists.');
                            validate = 1;
                            return false;
                        } else {
                            validate = 0;
                        }
                    },
                    error: function () {}
                });
            } else if (username != '' && validate == 0)
                validate = 0;
            else
                validate = 1;

            if (validate == 0)
            {
                var action = $('#hidAction').val();
                $('#addform').attr('action', action);
                $('#hidbtnsubmit').val("submit");
                $('#addform').submit();
            }
        }
        function showStateCity(obj) {
            var ss_id = obj.value;
            jQuery.ajax({
                url: "../includes/ajax_getUserAssLocation.php",
                data: 'ss_id=' + ss_id,
                type: "POST",
                async: false,
                success: function (data) {
                    var user_record = JSON.parse(data);
                    if (user_record != 0) {
                        var state = user_record.state;
                        var city = user_record.city;
                        $("#state").val(state);
                        setTimeout(
                                function ()
                                {
                                    fnShowCity(state);
                                    setTimeout(
                                            function ()
                                            {
                                                $("#city").val(city);
                                            }, 1800);
                                }, 600);
                        setTimeout(function ()
                        {
                            FnGetSuburbDropDown($("#city").get(0));
                        }, 2800);

                    }
                },
                error: function () {}
            });
        }
    </script>
    <!-- BEGIN FOOTER -->
<?php include "../includes/footer.php" ?>
    <!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>