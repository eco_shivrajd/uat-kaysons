<!-- BEGIN HEADER -->
<?php 
include "../includes/header.php";
//include "../../includes/config.php";

if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") {
	header("location:../logout.php");
} 

if(isset($_POST['hidbtnsubmit']))
{
	include "../includes/userManage.php";
	//print"<pre>";print_r($_POST);
	$userObj 	= 	new userManager($con,$conmain);
	$user_type	=	"Superstockist";
	/* Local user section */
	$userid_local = $userObj->addLocalUserDetails($user_type);
	$userObj->addLocalUserWorkingAreaDetails($userid_local);
	
	/* Common user section */
	$username	=	fnEncodeString($_POST['username']);
	$common_user_record = $userObj->getCommonUserDetailsByUsername($username);	
	if($common_user_record != 0)
	{
		$common_user_company_record = $userObj->getCommonUserCompanyDetails($userid_local);
		if($common_user_company_record != 0)
		{
			$userObj-addCommonUserCompanyDetails($userid_local);
		}
	}else{
		$userid_common = $userObj->addCommonUserDetails($user_type,$userid_local);
		$userObj->addCommonUserCompanyDetails($userid_local);
	}	
	$email		=	fnEncodeString($_POST['email']);
	if($email != '')
		$userObj->sendUserCreationEmail();
	
	echo '<script>alert("Superstockist added successfully.");location.href="superstockist.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Superstockist";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Superstockist</h3>
			
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="superstockist.php">Superstockist</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Superstockist</a>
					</li>
				</ul>				
			</div>
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Superstockist
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>						
				       <form class="form-horizontal" role="form" data-parsley-validate="" method="post" name="addform" id="addform">       
				           <?php $page_to_add = 'superstockist';include "userAddCommEle.php";	
				           //form common element file with javascript validation ?>    
				      
				            <div class="form-group">
				              <div class="col-md-4 col-md-offset-3">
								<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
								<input type="hidden" name="hidAction" id="hidAction" value="superstockist-add.php">
				                <button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
				                <a href="superstockist.php" class="btn btn-primary">Cancel</a>
				              </div>
				            </div><!-- /.form-group -->
				          </form>     
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
