<!-- BEGIN HEADER -->
<?php
include "../includes/header.php";
include "../includes/userManage.php";
$userObj = new userManager($con, $conmain);
?>
<!-- END HEADER -->
<?php
if (isset($_POST['hidbtnsubmit'])) {
    $id = $_POST['id'];
    $user_type = "DeliveryChannelPerson";
    $userObj->updateLocalUserDetails($user_type, $id);

    $working_detail = $userObj->getLocalUserWorkingAreaDetails($id);
    if ($working_detail == 0)
        $userObj->addLocalUserWorkingAreaDetails($id);
    else
        $userObj->updateLocalUserWorkingAreaDetails_dcp($id);

    $userObj->updateCommonUserDetails($id);
    $userObj->updateLocalUserOtherDetails($id);

    echo '<script>location.href="dcpusers.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix"></div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "DeliveryChannel";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <!-- /.modal -->
                <h3 class="page-title">Delivery Channel Person</h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="dcpusers.php">Delivery Channel Person</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Begin: life time stats -->
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                </div>
                                <a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']); ?>" class="btn btn-sm btn-default pull-right mt5">							
                                    Change Password
                                </a>
                            </div>
                            <div class="portlet-body">                              
                                <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                               
                                <?php
                                $id = $_GET['id'];
                                $userObj = new userManager($con, $conmain);
                                $user_details = $userObj->getLocalUserDetails($id);
                                $user_other_details = $userObj->getLocalUserOtherDetails($id);
                                $working_area_details = $userObj->getLocalUserWorkingAreaDetails($id);
                                if ($working_area_details != 0) {
                                    $row1 = array_merge($user_details, $working_area_details); //print"<pre>";print_r($row1);
                                    if ($row1['state_ids'] == '')
                                        $row1['state_ids'] = $user_details['state'];
                                    if ($row1['city_ids'] == '')
                                        $row1['city_ids'] = $user_details['city'];
                                }
                                else {
                                    $row1 = $user_details;
                                    $row1['state_ids'] = $user_details['state'];
                                    $row1['city_ids'] = $user_details['city'];
                                }

                                $parentid = $user_details['sstockist_id'];
                                ?>                       

                                <form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">

                                    <div class="form-group">
                                        <label class="col-md-3">Super Stockist:<span class="mandatory">*</span></label>
                                        <div class="col-md-4">
                                            <select name="cmbSuperStockist" id="cmbSuperStockist"  class="form-control">

                                                <?php
                                                $user_type1 = "Superstockist";
                                                $result1 = $userObj->getLocalUserDetailsByUserType($user_type1);
                                                while ($row = mysqli_fetch_array($result1)) {
                                                    $assign_id = $row['id'];
                                                    if ($parentid == $assign_id)
                                                        $sel = "SELECTED";
                                                    else
                                                        $sel = "";
                                                    echo "<option value='$assign_id' $sel>" . fnStringToHTML($row['firstname']) . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <!--new code-->
                                    <div class="form-group">
                                        <label class="col-md-3">Name:<span class="mandatory">*</span></label>
                                        <div class="col-md-4">
                                            <input type="text"
                                                   placeholder="Enter Name"
                                                   data-parsley-trigger="change"				
                                                   data-parsley-required="#true" 
                                                   data-parsley-required-message="Please enter name"
                                                   data-parsley-maxlength="50"
                                                   data-parsley-maxlength-message="Only 50 characters are allowed"				
                                                   name="firstname" class="form-control" 
                                                   value="<?php echo fnStringToHTML($row1['firstname']) ?>">
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label class="col-md-3">Username:<span class="mandatory">*</span></label>

                                        <div class="col-md-4">
                                            <input type="text" id="username"
                                                   placeholder="Enter Username"
                                                   data-parsley-minlength="6"
                                                   data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
                                                   data-parsley-maxlength="50"
                                                   data-parsley-maxlength-message="Only 50 characters are allowed"          
                                                   data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
                                                   data-parsley-required-message="Please enter Username, blank spaces are not allowed"
                                                   data-parsley-trigger="change"
                                                   data-parsley-required="true"
                                                   data-parsley-pattern="/^\S*$/" 
                                                   data-parsley-error-message="Username should be 6-50 characters without blank spaces"
                                                   name="username" class="form-control" 
                                                   value="<?php echo fnStringToHTML($row1['username']) ?>">
                                            <span id="user-availability-status"></span>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label class="col-md-3">Address:<span class="mandatory">*</span></label>

                                        <div class="col-md-4">
                                            <textarea name="address"
                                                      placeholder="Enter Address"
                                                      data-parsley-trigger="change"				
                                                      data-parsley-required="#true" 
                                                      data-parsley-required-message="Please enter address"
                                                      data-parsley-maxlength="200"
                                                      data-parsley-maxlength-message="Only 200 characters are allowed"							
                                                      rows="4" class="form-control"><?php echo fnStringToHTML($row1['address']) ?></textarea>
                                        </div>
                                    </div><!-- /.form-group -->


                                    <div class="form-group">
                                        <label class="col-md-3">State:<span class="mandatory">*</span></label>
                                        <div class="col-md-4">
                                            <?php
                                            $sql_state = "SELECT * FROM tbl_state where country_id=101";

                                            $result_state = mysqli_query($con, $sql_state);
                                            ?>
                                            <select name="state" id="state" class="form-control" 
                                                    data-parsley-trigger="change"
                                                    data-parsley-required="#true" 				
                                                    data-parsley-required-message="Please select State"
                                                    parsley-required="true"
                                                    data-parsley-required-message="Please select State"
                                                    onChange="fnShowCity(this.value)">
                                                        <?php
                                                        if ($row1['state_ids'] == 0)
                                                            echo "<option value=''>-Select-</option>";
                                                        while ($row_state = mysqli_fetch_array($result_state)) {
                                                            $selected = "";
                                                            if ($row_state['id'] == $row1['state_ids'])
                                                                $selected = "selected";

                                                            echo "<option value='" . $row_state['id'] . "' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
                                                        }
                                                        ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->

                                    <div class="form-group" id="city_div">
                                        <label class="col-md-3">District:<span class="mandatory">*</span></label>
                                        <div class="col-md-4" id="div_select_city">
                                            <?php
                                            $option = "<option value=''>-Select-</option>";
                                            if ($row1['city_ids'] != '') {
                                                $sql_city = "SELECT * FROM tbl_city where state_id='" . $row1['state_ids'] . "' ORDER BY name";

                                                $result_city = mysqli_query($con, $sql_city);
                                                while ($row_city = mysqli_fetch_array($result_city)) {
                                                    $cat_id = $row_city['id'];
                                                    $selected = "";
                                                    if ($row_city['id'] == $row1['city_ids'])
                                                        $selected = "selected";

                                                    $option .= "<option value='$cat_id' $selected>" . $row_city['name'] . "</option>";
                                                }
                                            }
                                            ?>
                                            <select name="city" id="city" class="form-control"  
                                                    data-parsley-trigger="change"
                                                    data-parsley-required="#true"	
                                                    data-parsley-required-message="Please select District"
                                                    onChange="FnGetSuburbDropDown(this)"><?= $option; ?></select>
                                        </div>
                                    </div><!-- /.form-group --> 
                                    <div class="form-group">
                                        <label class="col-md-3">Taluka:</label>
                                        <?php
                                        if ($row1['suburb_ids'] != '')
                                            $sql_area = "SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_surb WHERE id IN (" . $row1['suburb_ids'] . ") and isdeleted!='1' ";
                                        else
                                            $sql_area = "SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_surb WHERE cityid = '" . $row1['city_ids'] . "' and isdeleted!='1'";

                                        $result_area = mysqli_query($con, $sql_area);
                                        $suburb_array = explode(',', $row1['suburb_ids']);
                                        $multiple = '';
                                        if (mysqli_num_rows($result_area) > 1)
                                            $multiple = 'multiple';

                                        $selected = "";
                                        ?>
                                        <div class="col-md-4"  id="div_select_area">
                                            <select name="area[]" id="area" class="form-control" onchange="FnGetSubareaDropDown(this)">	
                                                <option value="">-Select-</option>
                                                <?php
                                                while ($row_area = mysqli_fetch_array($result_area)) {
                                                    if (count($suburb_array) > 0) {
                                                        if (in_array($row_area['id'], $suburb_array))
                                                            $selected = "selected";
                                                    }
                                                    echo "<option value='" . $row_area['id'] . "' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
                                                }
                                                ?>
                                            </select>				
                                        </div>
                                    </div>
                                    <input type="hidden" id="selected_taluka" name="selected_taluka" value="<?= $suburb_array[0]; ?>">
                                    <!-- /.form-group -->

                                    <div class="form-group" id="subarea_div">
                                        <label class="col-md-3">Subarea:</label>
                                        <?php
                                        $rows_subarea = 0;
                                        if (isset($row1['subarea_ids']) && $row1['subarea_ids'] != '') {
                                            $sql_subarea = "SELECT `subarea_id`, `state_id`, `city_id`, `suburb_id`, `subarea_name` 
					FROM tbl_subarea WHERE subarea_id IN (" . $row1['subarea_ids'] . ")";

                                            $result_subarea = mysqli_query($con, $sql_subarea);
                                            if ($result_subarea != '')
                                                $rows_subarea = mysqli_num_rows($result_subarea);
                                            $subarea_array = explode(',', $row1['subarea_ids']);
                                            $multiple = '';
                                            if (count($subarea_array) > 1)
                                                $multiple = 'multiple';

                                            $selected = '';
                                        }
                                        ?>
                                        <div class="col-md-4" id="div_select_subarea">
                                            <select name="subarea[]" id="subarea" data-parsley-trigger="change" class="form-control" >					
                                                <?php
                                                if ($rows_subarea == 0) {
                                                    echo '<option value="">-Select-</option>';
                                                } else {
                                                    while ($row_subarea = mysqli_fetch_array($result_subarea)) {
                                                        if (count($subarea_array) > 0) {
                                                            if (in_array($row_subarea['subarea_id'], $subarea_array))
                                                                $selected = "selected";
                                                        }
                                                        echo "<option value='" . $row_subarea['subarea_id'] . "' $selected>" . fnStringToHTML($row_subarea['subarea_name']) . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group --> 

                                    <div class="form-group">
                                        <label class="col-md-3">Email:</label>

                                        <div class="col-md-4">
                                            <input type="text" name="email" id="email" 
                                                   placeholder="Enter E-mail"
                                                   data-parsley-maxlength="100"
                                                   data-parsley-maxlength-message="Only 100 characters are allowed"
                                                   data-parsley-type="email"
                                                   data-parsley-type-message="Please enter valid e-mail"		   

                                                   data-parsley-trigger="change"

                                                   class="form-control" value="<?php echo fnStringToHTML($row1['email']) ?>">
                                        </div>
                                    </div><!-- /.form-group -->


                                    <div class="form-group">
                                        <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

                                        <div class="col-md-4">
                                            <input type="text"  name="mobile"
                                                   placeholder="Enter Mobile Number"
                                                   data-parsley-trigger="change"				
                                                   data-parsley-required="#true" 
                                                   data-parsley-required-message="Please enter mobile number"
                                                   data-parsley-minlength="10"
                                                   data-parsley-minlength-message="Mobile number should be of minimum 10 digits" 
                                                   data-parsley-maxlength="15"				    
                                                   data-parsley-maxlength-message="Only 15 digits are allowed"
                                                   data-parsley-pattern="^(?!\s)[0-9]*$"
                                                   data-parsley-pattern-message="Please enter numbers only"
                                                   class="form-control" value="<?php echo fnStringToHTML($row1['mobile']) ?>">
                                        </div>
                                    </div><!-- /.form-group -->
                                    <!--new code end -->

                                    <div class="form-group">
                                        <label class="col-md-3">Status:</label>
                                        <div class="col-md-4">
                                            <select name="user_status" id="user_status" class="form-control">
                                                <option value="Active" <?php if ($row1['user_status'] == 'Active') echo "selected"; ?>>Active</option>
                                                <option value="Inactive" <?php if ($row1['user_status'] == 'Inactive') echo "selected"; ?>>Inactive</option>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	
                                    <!--new code end-->

                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                                            <input type="hidden" name="hidAction" id="hidAction" value="dcpusers1.php">
                                            <input type="hidden" name="id" id="id" value="<?= $row1['id']; ?>">
                                            <? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
                                            <button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
                                            <? } ?>
                                            <a href="dcpusers.php" class="btn btn-primary">Cancel</a>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>  
                                <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width:300px;">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <p>
                                                <h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
                                                </p>                     
                                                <center><a href="sales_delete.php?id=<?php echo $row1['id'] ?>" ><button type="button" class="btn btn-success">Yes</button></a>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
                                                </center>
                                            </div>    
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include "../includes/footer.php" ?>
    <!-- END FOOTER -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        function fnShowCity(id_value) {
            $("#city_div").show();
            $("#area").html('<option value="">-Select-</option>');
            $("#subarea").html('<option value="">-Select-</option>');

            var param = '';
            var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory" + param;
            CallAJAX(url, "div_select_city");
        }
        function FnGetSuburbDropDown(id) {
            $("#area_div").show();
            var param = '';
            $("#subarea").html('<option value="">-Select-</option>');
            var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown" + param;
            CallAJAX(url, "div_select_area");
        }



        function calculate_data_count(element_value) {
            element_value = element_value.toString();
            var element_arr = element_value.split(',');
            return element_arr.length;
        }
        function setSelectNoValue(div, select_element) {
            var select_selement_section = '<select name="' + select_element + '" id="' + select_element + '" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
            document.getElementById(div).innerHTML = select_selement_section;
        }
        function FnGetSubareaDropDown(id) {
            var suburb_str = $("#area").val();
            $("#subarea").html('<option value="">-Select-</option>');
            if (suburb_str != null) {
                $("#subarea_div").show();
                var url = "getSubareaDropdown.php?area_id=" + id.value + "&select_name_id=subarea";
                CallAJAX(url, "div_select_subarea");

            } else {
                setSelectNoValue("div_select_subarea", "subarea");
            }
        }

        function checkAvailability() {
            $('#updateform').parsley().validate();
            var email = $("#email").val();
            var username = $("#username").val();
            var id = $("#id").val();
            if (username != '')
            {
                jQuery.ajax({
                    url: "../includes/checkUserAvailable.php",
                    data: 'validation_field=username&username=' + username + '&id=' + id,
                    type: "POST",
                    async: false,
                    success: function (data) {
                        if (data == "exist") {
                            alert('Username already exists.');
                            return false;
                        } else {
                            if (email != '')
                            {
                                jQuery.ajax({
                                    url: "../includes/checkUserAvailable.php",
                                    data: 'validation_field=email&email=' + email + '&id=' + id,
                                    type: "POST",
                                    async: false,
                                    success: function (data) {
                                        if (data == "exist") {
                                            alert('E-mail already exists.');
                                            return false;
                                        } else {
                                            submitFrm();
                                        }
                                    },
                                    error: function () {}
                                });
                            } else {
                                submitFrm();
                            }
                        }
                    },
                    error: function () {}
                });
            }
        }

        function submitFrm() {
            var action = $('#hidAction').val();
            $('#updateform').attr('action', action);
            $('#hidbtnsubmit').val("submit");
            $('#updateform').submit();
        }
    </script> 
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>