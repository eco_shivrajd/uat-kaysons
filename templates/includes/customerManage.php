<?php
/***********************************************************
 * File Name	: userManage.php
 ************************************************************/	
include "../includes/commonManage.php";	
class customerManager 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}
	public function getAllLocalUserDetails($user_type,$external_id=''){
		$where_clause = ''; 
		if($user_type == 'SalesPerson' && $_SESSION[SESSION_PREFIX.'user_type'] == "Superstockist" && $external_id != ''){
			$where_clause = " AND (sstockist_id = ". $external_id.")";
		}else if($external_id != '')
		{			
			$where_clause = " AND (external_id IN (". $external_id.") OR external_id LIKE ('%,". $external_id."%'))";
		}
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `suburbid`, `is_default_user`, `sstockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getLocalUserDetailsByUserType($user_type,$external_id='') {
		$where_clause = '';
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `suburbid`, `is_default_user`, `sstockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getCustomerDetails($user_id) {
		 $sql1="SELECT c.id, c.firstname, c.username, c.address, c.city, c.state, c.mobile,c.phone_no,c.email,c.accname,c.accno,c.accbrnm,c.accifsc,c.bank_name, c.gst_number_sss,s.name as state_name,ct.name as city_name
		FROM 
		tbl_customer c 
		LEFT JOIN tbl_state AS s ON s.id = c.state
        LEFT JOIN tbl_city AS ct ON ct.id = c.city
		where c.id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function get_cod_percent() {
		$sql1="SELECT cod_percent FROM tbl_cod_admin where status = 'active'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function updateLocalUserOtherDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		
		if($phone_no != '')
		{
			$values.= ", `phone_no` = '".$phone_no."'";
		}
		if($tollfree_no != '')
		{
			$values.= ", `tollfree_no` = '".$tollfree_no."'";
		}
		if($website != '')
		{
			$values.= ", `website` = '".$website."'";
		}
		if($declaration != '')
		{
			$values.= ", `declaration` = '".$declaration."'";
		}
		if($accname != '')
		{
			$values.= ", `accname` = '".$accname."'";
		}
		if($accno != '')
		{
			$values.= ", `accno` = '".$accno."'";
		}
		if($accbrnm != '')
		{
			$values.= ", `accbrnm` = '".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$values.= ", `accifsc` = '".$accifsc."'";
		}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".$bank_name."'";
		}
		if($gst_number_sss != '')
		{
			$values.= ", `gst_number_sss` = '".$gst_number_sss."'";
		}	
		$user_other_details = "UPDATE tbl_user_details SET userid='$user_id' $values WHERE userid='$user_id'";				
			
		mysqli_query($this->local_connection,$user_other_details);
		$this->commonObj->log_update_record('tbl_user_details',$user_id, $user_other_details);
	}
	public function addLocalUserOtherDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		$fields = '';		
		if($phone_no != '')
		{
			$fields.= ",`phone_no`";
			$values.= ",'".$phone_no."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";			
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";			
			$values.= ",'".$accno."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";			
			$values.= ",'".$bank_name."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";			
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";			
			$values.= ",'".$accifsc."'";
		}
		 $user_other_details = "INSERT INTO tbl_user_details (`userid` $fields) 
		VALUES( '".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_other_details);
		$this->commonObj->log_update_record('tbl_user_details',$user_id, $user_other_details);
	}
	public function assignRegion() {	
		extract ($_POST);	
		$region_id = $assign;			
		if(count($assign) > 1){
			$region_id = implode(',',$assign);
		}else{
			$region_id = $assign[0];
		}				
		
		$values.= " `suburb_ids`= '".$region_id."'";
		$update_user_sql = "UPDATE tbl_user_working_area SET $values WHERE user_id='$sales_p_id'";
		return mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user_working_area',$sales_p_id,$update_user_sql);
	}
	public function getLocalUserWorkingAreaDetails($user_id){
		$sql1="SELECT `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` FROM tbl_user_working_area where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;	
	}
	public function getCommonUserDetails($user_id) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where id = '".$user_id."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}

	public function getAdminDetails($user_id) {	
		 $sql1="SELECT UD.userid,U.gst_number_sss, UD.accname,  UD.accno, UD.accbrnm, UD.accifsc, UD.phone_no, UD.tollfree_no, UD.bank_name,U.gst_number_sss,UD.website, UD.declaration
		FROM tbl_user_details UD
        LEFT JOIN tbl_user U ON U.id = UD.userid
		where UD.userid = '".$user_id."' AND UD.isdeleted!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);	
		}else
			return $row_count;		
	}
	public function getLocalUserOtherDetails($user_id,$user_type='') {	
		$sql1="SELECT `userid`, `accname`,  `accno`, `accbrnm`, `accifsc`, `phone_no`, `tollfree_no`, `bank_name`,`gst_number_sss`,`website`, `declaration`
		FROM tbl_user_details where userid = '".$user_id."' AND isdeleted!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);	
		}else
			return $row_count;		
	}

		public function getLocalUserWorkingRegionDetails($user_id) {
		$sql1="SELECT `user_id`, `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids`,
		(SELECT name FROM tbl_state WHERE id = tbl_user_working_area.state_ids) AS state_name,
		(SELECT name FROM tbl_city WHERE id = tbl_user_working_area.city_ids) AS city_name,
		(SELECT  suburbnm FROM tbl_surb WHERE id = tbl_user_working_area.suburb_ids) AS region_name
		FROM `tbl_user_working_area`
		where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	  }
	public function getCustomerDetailsByUsername($username,$password) {
		$password1 = md5($password);
	    $sql1="SELECT `id`,`pwd` FROM tbl_customer where username='".$username."' AND pwd='".$password1."'";
		$result1 = mysqli_query($this->local_connection,$sql1);       
	    $row_count = mysqli_num_rows($result1);	
			return $row_count;			
	}
	public function getCommonUserCompanyDetails($user_id) {
		$sql1="SELECT `id`, `userid`, `companyid` FROM tbl_user_company where userid='".$user_id."' AND companyid='".COMPID."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;
	}
	public function addCustomerDetails($user_type) {
		extract ($_POST);
		//print_r($_POST);
		//exit();
		$external_id =	$_SESSION[SESSION_PREFIX.'user_id'];
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		
		$fields = '';
		$values = ''; 
		if($user_type != '')
		{
			$fields.= ",`user_type`";
			$values.= ",'".$user_type."'";
		}
		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";
			$values.= ",'".$accno."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";
			$values.= ",'".$accifsc."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";
			$values.= ",'".$bank_name."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}		
		 $user_sql = "INSERT INTO tbl_customer (`external_id`,`firstname`,`username`,`pwd` $fields) 
		VALUES('".$external_id."','".$name."','".$username."','".$password."' $values)";
		
		mysqli_query($this->local_connection,$user_sql);		
		//$userid=mysqli_insert_id($this->local_connection);
		//echo $userid;
		$this->commonObj->log_add_record('tbl_customer',$user_sql);	
		return $userid;
	}

 public function sendCustmorCreationEmail($username,$password,$email,$firstname)
    {
    	$to  = $email;
    	//$cc = "Deepak.Oza@ecotechservices.com,Samayasri.V@ecotechservices.com";
    	$replay = "supply@salzpoint.net";
    	//  $to = "somebody@example.com, Deepak.Oza@ecotechservices.com";
		if($to!="")
		{
	      // $to = Email;
 $subject = AccountDetails ;
 $body = "<p>Hello $firstname,</p>          
            <p>
				Your login details are : <br/>
				URL : <a href='".SITEURL."/templates/login.php'>Click Here</a> to Login <br/>
				User name : $username <br/>
				Password : $password  
			</p>
			<br/>
			Thanks,<br/>
			Admin.";

    $headers = 'From: supply@salzpoint.net' . "\r\n" ;
    $headers .='Reply-To: '. $replay . "\r\n" ;
    $headers .='CC: '.$cc ."\r\n";
    $headers .='X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   
		  if(mail($to,$subject, $body,$headers)) 
		  {
		  //echo('<br>'."Email Sent ;".'</br>');
		  return true;
		  } 
		  else 
		  {
		  echo("<p>Email Message delivery failed...</p>");
		  }
		}
    }

	public function addLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state_ids`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city_ids`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		
		$user_working_area = "INSERT INTO tbl_user_working_area (`user_id` $fields) 
		VALUES('".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_working_area);		
		$this->commonObj->log_add_record('tbl_user_working_area',$user_id,$user_working_area);	
	}
	public function addCommonUserDetails($user_type,$user_id) {
		extract ($_POST);
		$email		=	fnEncodeString($email);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		$user_sql = "INSERT INTO tbl_users (`id`, `emailaddress`, `passwd`, `username`, `level` $fields) 
		VALUES('".$user_id."','".$email."','".$password."','".$username."','".$user_type."' $values)";
		mysqli_query($this->common_connection,$user_sql);	
		return $userid=mysqli_insert_id($this->common_connection); 		
	}
	public function addCommonUserCompanyDetails($user_id) {		
		$company_sql = "INSERT INTO tbl_user_company(userid,companyid)  VALUES('".$user_id."','".COMPID."')";
		mysqli_query($this->common_connection,$company_sql);
	}
	public function sendCustCreationEmail() {
		extract ($_POST);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$subject   = "Account Created";
		$fromMail  = FROMMAILID;		
		$headers = "";
		$headers .= "From: ".$fromMail."\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-Type: text/html;" . "\r\n";
		$message = "";
		$message .= "Hi, <br/>";
		$message .= "Your account created successfully<br/>";
		$message .= "Please find the login details below<br/>";
		$message .= "Username: $username<br/>";
		$message .= "Password: $rand<br/>";
		$message .= "<a href='".SITEURL."templates/login.php'>Click Here</a> to Login<br/>";
		$message .= "<br/><br/>";
		$message .= "Thanks,<br/>";
		$message .= "Admin.";
		if($username != ""){
			$sent = @mail($username,$subject,$message,$headers);
		}
	}
	public function updateLocalUserDetails($user_type, $user_id) {

		extract ($_POST);	
		$address= fnEncodeString($address);	
		$email	= fnEncodeString($email);
		$mobile	= fnEncodeString($mobile);			
		$values = '';
		
		$values.= " `id`= '".$user_id."'";
			
		if($address != '')
		{
			$values.= ", `address`= '".$address."'";				
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		if($email != '')
		{
			$values.= ", `email`= '".$email."'";				
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($phone_no != '')
		{
			$values.= ", `phone_no`= '".$phone_no."'";				
		}
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson') && $assign != "")
		{
			$external_id = $assign;
			if($user_type == 'SalesPerson'){
				if(count($assign) > 1){
					$external_id = implode(',',$assign);
				}else{
					$external_id = $assign[0];
				}	
				
			}
			$values.= ", `external_id`= '".$external_id."'";
		}else if($user_type == 'SalesPerson' && $assign == ""){
			$values.= ", `external_id`= ''";
		}
		if($user_type == 'SalesPerson'){
			$values.= ", `sstockist_id`= ".$cmbSuperStockist;
		}
		if($user_type == 'Superstockist'||$user_type =='Distributor'){
			$values.= ", `gst_number_sss`= '".$gstnumber."'";
		}
		//if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist' OR $_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		//{
			if($firstname!="") {
				$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";	
			}
			if($username!="") {
				$values.= ", `username`= '".fnEncodeString($username)."'";					
			}
			
		//}
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){
			if($cod_percent!="") {
				$update_cod_sql = "update  tbl_cod_admin SET status='inactive'";
				mysqli_query($this->local_connection,$update_cod_sql);
				
				$insert_cod_sql = "insert  tbl_cod_admin SET cod_percent='$cod_percent',status='active'";
				mysqli_query($this->local_connection,$insert_cod_sql);
			}
		}
		
		$update_user_sql = "UPDATE tbl_user SET $values WHERE id='$user_id'";
		mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$user_id,$update_user_sql);
	}
	
	public function updateCustomerDetails($user_id) {		
		extract ($_POST);	
		//print_r($_POST);
		//exit();
		if($firstname != '')
		{
			$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values.= ", `username`= '".fnEncodeString($username)."'";
		}
		if($email != '')
		{
			$values.= ", `email`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		if($gstnumber != '')
		{	
			$values.= ",`gst_number_sss`= '".$gstnumber."'";
		}
		if($accname != '')
		{	
			$values.= ",`accname`= '".$accname."'";
		}
		if($accno != '')
		{	
			$values.= ",`accno`= '".$accno."'";
		}
		if($bank_name != '')
		{	
			$values.= ",`bank_name`= '".$bank_name."'";
		}
		if($accbrnm != '')
		{	
			$values.= ",`accbrnm`= '".$accbrnm."'";
		}
		if($accifsc != '')
		{	
			$values.= ",`accifsc`= '".$accifsc."'";
		}
	   $update_user_sql = "UPDATE tbl_customer SET id='$user_id' $values WHERE id='$user_id'";
		
		mysqli_query($this->local_connection,$update_user_sql);		
	}
	
	public function updateLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		if($state != '')
		{
			$values.= ", `state_ids`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city_ids`= '".$city."'";
		}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids`= '".$suburb_ids."'";
		}else{
			$values.= ", `suburb_ids`= ''";
		}
		if($subarea != '')
		{			
			$subarea_ids = implode(',',$subarea);
			$values.= ",`subarea_ids`= '".$subarea_ids."'";
		}else{
			$values.= ",`subarea_ids`= ''";
		}
			
		$user_working_area = "UPDATE tbl_user_working_area SET user_id='$user_id' $values WHERE user_id='$user_id'";				
			
		mysqli_query($this->local_connection,$user_working_area);
		$this->commonObj->log_update_record('tbl_user_working_area',$user_id, $user_working_area);
	}

	public function deleteCustomerbyid($id){
		$tbl_customer = "UPDATE  tbl_customer SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_customer);
		
	}
	
	public function getAllLocalUser($user_type){
		$where="";
		switch($_SESSION[SESSION_PREFIX.'user_type']){
		case "Admin":				
				$where.=" ";
			break;
			case "Superstockist":			
				$where.=" AND tbl_user.sstockist_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";
			break;
			case "Distributor":
				$where.=" AND tbl_user.external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";				
			break;
		}
		 $sql1="SELECT `id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `suburbid`, `is_default_user`
		FROM tbl_user where user_type = '".$user_type."' 
		$where
		order by is_default_user desc, firstname asc";//, `user_status`
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}	

}
?>